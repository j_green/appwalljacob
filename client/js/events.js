Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postImage = event.currentTarget.children[0].files[0];

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                //add in code to tell the user the upload failed
            } else {
                //add code to insert post data
                //insert data: name, message, imageId, created by date
                //fileObject._id
                $('.grid').masonry('reloadItems');
            }
        });
    }
});